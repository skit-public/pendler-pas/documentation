# Popis Aplikácie

Hlavným cieľom aplikácie je uľahčiť pendlerom prechod hranicami pomocou overeného zeleného pendler pasu a vyhnúť sa tak dlhým kontrolám na hraniciach. Aplikácia funguje v offline režime s výnimkou registrácie a aby dokázala pendlera overiť, musí používateľ povoliť lokalizáciu, čím dáva súhlas so zaznamenávaním jeho polohy. Poloha sa zaznamenáva len v zariadení pendlera.

Aplikácia pozostáva z registračného formulára, kde sú k dispozícií pravidlá používania s ktorými musí používateľ súhlasiť aby mohol pokračovať ďalej. Nastavením pinkódu aplikáciu zabezpečíme a po úspešnej registrácií sa používateľ dostáva na hlavnú obrazovku kde vidí svoje základné informácie ako meno, priezvisko, čislo OP a penlder pas ktorý je v stave overovania. Na základe algoritmu sa pendler pas overuje na pozadí aplikácie. Pravidelné notifikácie zabezpečujú, aby používateľ aplikáciu otvoril a overil tak svoju polohu. V prípade bližiacej sa expirácie pendler pasu, aplikácia používateľa upozorní na danú expiráciu pomocou notifikácie. Ak používateľ splní dané pravidla, stáva sa overeným pendlerom a môže tak pomocou pendler pasu pendlovať medzi vopred zadanými krajinami.
 

# Technické riešenie

Aplikácia je založená na skúmaní a vyhodnocovaní polohy používateľa. Poloha je zisťovaná pomocou internetu, GPS a mobilnej siete pričom je ukladaná do lokálnej databázy na zariadení a nie je odosielaná na žiadny server. Tieto údaje následne slúžia na vyhodnocovanie platnosti pendler pasu používateľa. Všetky pravidlá pre vyhodnocovanie platnosti pendler pasu sú uložené vo Firebase Remote Config. Obe platformy majú tiež zabezpečenie proti tzv. "Root access". Internet je vyžadovaný počas registrácie len za účelom stiahnutia aktuálnych konfigurácií z Firebase Remote Config. Krajina je vyhodnocovaná na základe polohy používateľa pomocou hraníc definovaných v súbore geo_codes.json.

- geo_codes.json: https://gitlab.com/skit-public/pendler-pas/documentation/-/blob/master/geo_codes.json

### Android

Natívne riešenie pre platformu Android je vytvorené v jazyku Kotlin a je založené na architektúre MVVM, databáze Room, vzdialenej konfigurácií Firebase Remote Config a komponenty OfflineGeocoder pre získavanie polohy keď je zariadenie v režime offline. Implementovaná je tiež funkcionalita pre zisťovanie simulovania polohy. Údaje používateľa sú šifrované v databáze pomocou SQLCipher, pri Shared Preferences je použitá natívna implementácia zabezpečenia od Androidu.

- Kotlin: https://kotlinlang.org/
- MVVM: https://developer.android.com/jetpack/guide?gclid=CjwKCAjw7J6EBhBDEiwA5UUM2iFMakRgCRsktx87O83mU12WapLTXTJC6hQvWQawDZ9zGEieqQruUBoCIiMQAvD_BwE&gclsrc=aw.ds
- Room: https://developer.android.com/training/data-storage/room
- Firebase Remote Config: https://firebase.google.com/docs/remote-config
- Encrypted Shared Preferences: https://developer.android.com/topic/security/data
- SQLCipher: https://github.com/sqlcipher/android-database-sqlcipher
- OfflineGeocoder: https://github.com/roisg/OfflineGeocoder-Android

### iOS

Natívne riešenie pre platformu iOS je vytvorené v jazyku Swift a je určené pre iba pre mobilné telefóny značky iPhone. Aplikácia je založená na architektúre MVVM, databáze RealmSwift a vzdialenej konfigurácií Firebase Remote Config. Na získavanie aktuálnej polohy používa komponent tretej strany OfflineGeocoder, ktorý umožňuje určiť aktuálnu polohu ak je telefón v offline režime. Údaje používateľa ako aj záznamy o polohe sú ukladané v databáze a v heslom zabezpečenom Keychain úložisku.

- Swift: https://developer.apple.com/swift/
- RealmSwift: hhttps://cocoapods.org/pods/RealmSwift
- Firebase Remote Config: https://firebase.google.com/docs/remote-config
- OfflineGeocoder: https://github.com/soheilbm/IOS-Offline-GeoCoder

# Zdrojové kódy

Dostupné na vyžiadanie:
- Android: https://gitlab.com/pendlerpass/pendlerpass_android_app
- iOS: https://gitlab.com/pendlerpass/pendlerpass_ios_app
